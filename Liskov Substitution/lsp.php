<?php

// violation of LS principle.
interface UserRepository
{
    /**
     * @return collection
     */
    public function getUserData($userId);
}

class NormalUserRepository implements UserRepository
{
    // Returns collection
    public function getUserData($userId)
    {
        return DB::table('users')->where('user_id', '=', $userId);
    }
}

class ThirdPartyUserRepository implements UserRepository
{
    // Returns array
    public function getUserData($userId)
    {
        return Filesystem::getUserData($userId);
    }
}
}