<?php


interface OnlinePaymentInterface
{
    public function pay();

    public function verify();
}


interface OfflinePaymentInterface
{

    public function pay();
}

class OnlinePayment implements OnlinePaymentInterface
{
    public function pay()
    {

    }

    public function verify()
    {

    }
}

/* in `cartToCart class we dont need to
 implement verify method.
 not implementing verify method
 violates LS principle.
 if we have not to implement some method,
 but actually we have the method stubs,
 we have violated the lsp.
 we should separate the interface
*/
class CartToCart implements OfflinePaymentInterface
{
    public function pay()
    {
        // logic
    }

    public function verify()
    {

    }
}

/*
Classes that do implement an interface must be same in all aspect,
method signature, input, output, etc.
using if is a bad habit.
*/

interface Database
{
    public function getAll() : array;
}


class Mysql implements Database
{
    public function getAll(): array
    {

    }
}

class NoSql implements Database
{
    // False ! it's clearly False!
    // return type is not consistent.
    public function getAll(): json
    {

    }
}