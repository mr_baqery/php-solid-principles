## Proxy

### Purpose

To interface to anything that is expensive or impossible to duplicate.

### Examples

Doctrine2 uses proxies to implement framework magic (e.g. lazy initialization) in them, while the user still works with
his own entity classes and will never use nor touch the proxies

### Intent

Proxy is a structural design pattern that lets you provide a substitute or placeholder for another object. A proxy
controls access to the original object, allowing you to perform something either before or after the request gets
through to the original object.