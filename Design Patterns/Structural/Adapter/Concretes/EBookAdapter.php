<?php


namespace DesignPatterns\Structural\Adapter;


class EBookAdapter implements Book
{
    public function __construct(protected EBook $eBook)
    {
    }

    public function turnPage()
    {
        $this->eBook->pressNext();
    }

    public function open()
    {
        $this->eBook->unlock();
    }

    public function getPage(): int
    {
        $this->eBook->getPage();
    }
}