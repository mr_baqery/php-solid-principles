## Adapter / Wrapper

### Purpose

To translate one interface for a class into a compatible interface. An adapter allows classes to work together that
normally could not because of incompatible interfaces by providing its interface to clients while using the original
interface.

### Examples

DB Client libraries adapter using multiple different webservices and adapters normalize data so that the outcome is the
same for all

![](https://res.cloudinary.com/practicaldev/image/fetch/s--twUjlcQI--/c_limit%2Cf_auto%2Cfl_progressive%2Cq_auto%2Cw_880/https://cdn-images-1.medium.com/max/1000/0%2ATp5pwOgs7tytX9_w.png)

![](https://miro.medium.com/max/566/1*E1oLGrommAKv5_XWSVBD4A.jpeg)

![](https://i1.wp.com/www.javagists.com/wp-content/uploads/2018/01/adapter-Example.png?w=688&ssl=1)