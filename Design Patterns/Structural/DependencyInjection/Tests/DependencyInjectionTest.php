<?php

namespace DesignPatterns\Structural\DependencyInjection\Tests;

use DesignPatterns\Structural\DependencyInjection\Concretes\DatabaseConfiguration;
use DesignPatterns\Structural\DependencyInjection\Concretes\DatabaseConnection;
use PHPUnit\Framework\TestCase;

class DependencyInjectionTest extends TestCase
{
    public function testDependencyInjection()
    {
        $config = new DatabaseConfiguration("localhost", "3306", "root", "1234");

        $connection = new DatabaseConnection($config);

        $this->assertSame("root:1234@localhost:3306", $connection);
    }
}