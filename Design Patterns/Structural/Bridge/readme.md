## Bridge

### Purpose Decouple an abstraction from its implementation so that the two can vary independently.

![](https://lh3.googleusercontent.com/proxy/uTQOfbkGWx-0OJIIMTEcj5vm5fovLWxXHud8J9OGJbsdrA4LglqGoGQCS2njbUuCTAnkFFszcVlHzycHTwRaPXbM_vofo0hyeuwQ-985L0Qn1eY2RVIObyfGFj4i6SgNw3RhxIWQnflh-qhRDKSFYynDYGTuLw)