<?php

namespace DesignPatterns\Structural\Bridge;

class PingService extends Service
{

    public function get(): string
    {
        $this->implementation->format("pong");
    }
}