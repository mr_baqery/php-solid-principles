<?php


use DesignPatterns\Structural\Facade\Contracts\Bios;
use DesignPatterns\Structural\Facade\Contracts\OperatingSystem;

class Facade
{
    public function __construct(
        private Bios $bios,
        private OperatingSystem $os
    )
    {
    }

    public function turnOn()
    {
        $this->bios->execute();
        $this->bios->waitForKeyPress();
        $this->bios->launch($this->os);
    }

    public function turnOff()
    {
        $this->os->halt();
        $this->bios->powerDown();
    }
}