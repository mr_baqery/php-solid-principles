<?php

namespace DesignPatterns\Structural\Facade\Contracts;

interface OperatingSystem
{
    public function halt();

    public function getName(): string;
}