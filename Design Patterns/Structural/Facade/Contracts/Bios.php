<?php

namespace DesignPatterns\Structural\Facade\Contracts;

interface Bios
{
    public function execute();

    public function waitForKeyPress();

    public function launch(OperatingSystem $os);

    public function powerDown();
}