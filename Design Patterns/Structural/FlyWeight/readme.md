## Flyweight

### Purpose

To minimise memory usage, a Flyweight shares as much as possible memory with similar objects. It is needed when a large
amount of objects is used that don’t differ much in state. A common practice is to hold state in external data
structures and pass them to the flyweight object when needed.

![](https://upload.wikimedia.org/wikipedia/commons/4/4e/W3sDesign_Flyweight_Design_Pattern_UML.jpg)

![](https://www.researchgate.net/profile/Andres-Pablo-Flores/publication/277288211/figure/fig5/AS:669501678972943@1536633047663/Flyweight-Pattern-Structure-jects-to-have-ConcreteFlyweight-objects-as-children-at-some.png)

