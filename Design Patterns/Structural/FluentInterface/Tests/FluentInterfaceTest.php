<?php

namespace DesignPatterns\Structural\FluentInterface\Tests;

use DesignPatterns\Structural\FluentInterface\Person;
use DesignPatterns\Structural\FluentInterface\Sql;
use PHPUnit\Framework\TestCase;

class FluentInterfaceTest extends TestCase
{
    public function testBuildSql()
    {
        $query = (new Sql())
            ->select(['foo', 'bar'])
            ->from('foobar', 'f')
            ->where('f.bar = ?');

        $this->assertSame('SELECT foo, bar FROM foobar AS f WHERE f.bar = ?', (string)$query);

    }

    public function testBuildPerson()
    {
        $person = (new Person())
            ->setName("Foo")
            ->setLastName("Bar")
            ->setAge(44);

        $this->assertSame("Foo Bar 44", (string)$person);
    }

}