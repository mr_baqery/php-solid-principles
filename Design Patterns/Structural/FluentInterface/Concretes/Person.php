<?php


namespace DesignPatterns\Structural\FluentInterface;


class Person implements \Stringable
{
    protected string $name;
    protected string $last_name;
    protected string $age;

    public function setName(string $name): Person
    {
        $this->name = $name;
        return $this;
    }

    public function setLastName(string $lastName): Person
    {
        $this->last_name = $lastName;
        return $this;
    }

    public function setAge(int $age): Person
    {
        $this->age = (string)$age;
        return $this;
    }

    public function __toString()
    {
        return sprintf("%s %s %s",
            $this->name,
            $this->last_name,
            $this->age
        );
    }
}