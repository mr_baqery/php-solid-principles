## Composite

### Purpose

To treat a group of objects the same way as a single instance of the object.

### Examples

a form class instance handles all its form elements like a single instance of the form, when render() is called, it
subsequently runs through all its child elements and calls render() on them

Using the Composite pattern makes sense only when the core model of your app can be represented as a tree.

For example, imagine that you have two types of objects: Products and Boxes. A Box can contain several Products as well as a number of smaller Boxes. These little Boxes can also hold some Products or even smaller Boxes, and so on.


![](https://refactoring.guru/images/patterns/diagrams/composite/problem-en-2x.png?id=5c7d443ccce3e46c4308)

![](https://refactoring.guru/images/patterns/diagrams/composite/structure-en-2x.png?id=fc41be8ae17c7250ea6d)

![](https://upload.wikimedia.org/wikipedia/commons/6/65/W3sDesign_Composite_Design_Pattern_UML.jpg)