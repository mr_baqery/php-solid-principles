<?php


namespace DesignPatterns\Structural\DataMapper;

use InvalidArgumentException;


class UserMapper
{
    public function __construct(
        private StorageAdapter $adapter
    )
    {
    }

    public function findById(int $id): User
    {
        $result = $this->adapter->find($id);

        if ($result === null) {
            throw new \http\Exception\InvalidArgumentException("User did not found");
        }

        return $this->mapRowToUser($result);
    }

    private function mapRowToUser(array $row): User
    {
        return User::fromState($row);
    }
}