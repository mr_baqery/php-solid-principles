<?php


namespace DesignPatterns\Structural\Decorator;


class Wifi extends BookingDecorator
{

    private const PRICE = 5;
    private const TAXES = 1;

    public function calculatePrice(): int
    {
        $this->booking->calculatePrice() + Wifi::PRICE;
    }

    public function getDescription(): string
    {
        $this->booking->getDescription() . " with a wifi";
    }

    public function checkout(): int
    {
        return $this->calculatePrice() + Wifi::TAXES;
    }
}