<?php


namespace DesignPatterns\Structural\Decorator;


class ExtraBed extends BookingDecorator
{
    private const PRICE = 30;
    private const taxes = 10;

    public function calculatePrice(): int
    {
        return $this->booking->calculatePrice() + ExtraBed::PRICE;
    }

    public function getDescription(): string
    {
        return $this->booking->getDescription() . ' with two extra bed';
    }

    public function checkout(): int
    {
        return $this->calculatePrice() + ExtraBed::taxes;
    }
}