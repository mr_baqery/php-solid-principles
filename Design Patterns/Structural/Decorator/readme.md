## Decorator
### Purpose
To dynamically add new functionality to class instances.

### Examples
Web Service Layer: Decorators JSON and XML for a REST service (in this case, only one of these should be allowed of course)


![](https://i.stack.imgur.com/MvWeI.png)
![](https://www.oreilly.com/library/view/learning-javascript-design/9781449334840/httpatomoreillycomsourceoreillyimages1547817.png)
