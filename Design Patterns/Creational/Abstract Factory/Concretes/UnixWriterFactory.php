<?php


namespace DesignPatters\AbstractFactory\Concretes;


use DesignPatterns\AbstractFactory\Contracts\CsvWriter;
use DesignPatterns\AbstractFactory\Contracts\JsonWriter;
use DesignPatterns\AbstractFactory\Contracts\WriterFactory;

class UnixWriterFactory implements WriterFactory
{
    public function createJsonWriter(): JsonWriter
    {
        return new UnixJsonWriter();
    }

    public function createCsvWriter(): CsvWriter
    {
        return new UnixCsvWriter();
    }
}