<?php


namespace DesignPatters\AbstractFactory\Concretes;

use DesignPatterns\AbstractFactory\Contracts\CsvWriter;
use DesignPatterns\AbstractFactory\Contracts\JsonWriter;
use DesignPatterns\AbstractFactory\Contracts\WriterFactory;

class WinWriterFactory implements WriterFactory
{
    public function createJsonWriter(): JsonWriter
    {
        return new WinJsonWriter();
    }

    public function createCsvWriter(): CsvWriter
    {
        return new WinCsvWriter();
    }
}