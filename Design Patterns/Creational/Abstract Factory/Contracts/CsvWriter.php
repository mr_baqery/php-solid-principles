<?php


namespace DesignPatterns\AbstractFactory\Contracts;


interface CsvWriter
{
    public function write(array $line): string ;
}