<?php


namespace DesignPatterns\AbstractFactory\Contracts;


interface WriterFactory
{
    public function createJsonWriter(): JsonWriter;

    public function createCsvWriter(): CsvWriter;
}