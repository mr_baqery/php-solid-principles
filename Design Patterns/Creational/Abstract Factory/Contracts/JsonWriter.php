<?php


namespace DesignPatterns\AbstractFactory\Contracts;


interface JsonWriter
{
    public function write(array $data, bool $formatted): string;
}