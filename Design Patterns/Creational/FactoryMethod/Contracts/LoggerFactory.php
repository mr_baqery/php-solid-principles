<?php


namespace DesignPatters\FactoryMethod\Contracts;


interface LoggerFactory
{
    public function createLogger(): Logger;
}