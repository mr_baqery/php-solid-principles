<?php

namespace DesignPatters\FactoryMethod\Contracts;

interface Logger
{
    public function log(string $message);
}