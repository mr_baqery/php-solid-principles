<?php


namespace DesignPatters\FactoryMethod\Tests;


use DesignPatters\FactoryMethod\Concretes\FileLogger;
use DesignPatters\FactoryMethod\Concretes\FileLoggerFactory;
use DesignPatters\FactoryMethod\Concretes\StdoutLogger;
use DesignPatters\FactoryMethod\Concretes\StdoutLoggerFactory;
use PHPUnit\Framework\TestCase;


class Tests extends TestCase
{
    public function testCanCreateStdoutLogging()
    {
        $loggerFactory = new StdoutLoggerFactory();
        $logger = $loggerFactory->createLogger();

        $this->assertInstanceOf(StdoutLogger::class, $logger);
    }

    public function testCanCreateFileLogger()
    {
        $loggerFactory = new FileLoggerFactory(sys_get_temp_dir());
        $logger = $loggerFactory->createLogger();

        $this->assertInstanceOf(FileLogger::class, $logger);
    }

}