<?php


namespace DesignPatters\FactoryMethod\Concretes;


use DesignPatters\FactoryMethod\Contracts\Logger;
use DesignPatters\FactoryMethod\Contracts\LoggerFactory;

class StdoutLoggerFactory implements LoggerFactory
{
    public function createLogger(): Logger
    {
        return new StdoutLogger();
    }
}