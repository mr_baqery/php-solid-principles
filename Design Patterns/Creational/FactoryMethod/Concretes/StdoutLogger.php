<?php


namespace DesignPatters\FactoryMethod\Concretes;


use DesignPatters\FactoryMethod\Contracts\Logger;

class StdoutLogger implements Logger
{
    public function log(string $message)
    {
        echo $message;
    }
}