<?php


namespace DesignPatters\FactoryMethod\Concretes;


use DesignPatters\FactoryMethod\Contracts\Logger;
use DesignPatters\FactoryMethod\Contracts\LoggerFactory;

class FileLoggerFactory implements LoggerFactory
{
    public function __construct(private string $filePath)
    {
    }

    public function createLogger(): Logger
    {
        return new FileLogger($this->filePath);
    }
}