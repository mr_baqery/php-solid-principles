<?php


namespace DesignPatters\Pool\Concretes;


class WorkerPool implements \Countable
{
    /**
     * Occupied worker
     * @var array
     */
    private array $occupiedWorkers = [];

    private array $freeWorker = [];

    public function get(): StringReverseWorker
    {
        if (count($this->freeWorker) == 0) {
            $worker = new StringReverseWorker();
        } else {
            $worker = array_pop($this->freeWorker);
        }

        $this->occupiedWorkers[spl_object_hash($worker)] = $worker;

        return $worker;
    }

    public function dispose(StringReverseWorker $worker)
    {
        $key = spl_object_hash($worker);

        if (isset($this->occupiedWorkers[$key])) {
            unset($this->occupiedWorkers[$key]);
            $this->freeWorker[$key] = $worker;
        }
    }

    public function count(): int
    {
        return count($this->freeWorker) + count($this->occupiedWorkers);
    }
}