<?php


namespace DesignPatters\Builder\Tests;

use DesignPatters\Builder\Concretes\CarBuilder;
use DesignPatters\Builder\Concretes\Director;
use DesignPatters\Builder\Concretes\Parts\Car;
use DesignPatters\Builder\Concretes\Parts\Truck;
use DesignPatters\Builder\Concretes\TruckBuilder;
use PHPUnit\Framework\TestCase;


class BuilderTests extends TestCase
{
    public function testCanBuildTruck()
    {
        $truckBuilder = new TruckBuilder();
        $newVehicle = (new Director())->build($truckBuilder);

        $this->assertInstanceOf(Truck::class, $newVehicle);
    }

    public function testCanBuildCar()
    {
        $carBuilder = new CarBuilder();
        $newVehicle = (new Director())->build($carBuilder);

        $this->assertInstanceOf(Car::class, $newVehicle);
    }
}