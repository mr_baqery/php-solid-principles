<?php


namespace DesignPatters\Builder\Concretes;


use DesignPatters\Builder\Concretes\Parts\Door;
use DesignPatters\Builder\Concretes\Parts\Engine;
use DesignPatters\Builder\Concretes\Parts\Truck;
use DesignPatters\Builder\Concretes\Parts\Wheel;
use DesignPatters\Builder\Contracts\Builder;

class TruckBuilder implements Builder
{
    private Truck $truck;

    // Violation of dependency inversion principle
    public function createVehicle()
    {
        $this->truck = new Truck();
    }

    public function addWheel()
    {
        $this->truck->setPart("wheel", new Wheel());
    }

    public function addEngine()
    {
        $this->truck->setPart("engine", new Engine());
    }

    public function addDoors()
    {
        $this->truck->setPart("door", new Door());
    }

    public function getVehicle(): Truck
    {
        return $this->truck;
    }
}