<?php


namespace DesignPatters\Builder\Concretes;


use DesignPatters\Builder\Concretes\Parts\Car;
use DesignPatters\Builder\Contracts\Builder;

class CarBuilder implements Builder
{
    private Car $car;

    // Violation of dependency inversion principle
    public function createVehicle()
    {
        $this->car = new Car();
    }

    public function addWheel()
    {
        $this->car->setPart("leftWheel", new Wheel());
        $this->car->setPart("rightWheel", new Wheel());
    }

    public function addEngine()
    {
        $this->car->setPart("Engine", new Engine());
    }

    public function addDoors()
    {
        $this->car->setPart("Doors", new Door());
    }

    public function getVehicle(): Car
    {
        return $this->car;
    }
}