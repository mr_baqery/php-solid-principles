<?php


namespace DesignPatters\SimpleFactory\Tests;

use DesignPatters\SimpleFactory\Concretes\Bicycle;
use DesignPatters\SimpleFactory\Concretes\SimpleFactory;
use PHPUnit\Framework\TestCase;

class SimpleFactoryTests extends TestCase
{
    public function testCanCreateBicycle()
    {
        $bicycle = (new SimpleFactory())->createBicycle();
        $this->assertInstanceOf(Bicycle::class, $bicycle);
    }
}