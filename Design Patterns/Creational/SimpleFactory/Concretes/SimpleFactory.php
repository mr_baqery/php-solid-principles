<?php


namespace DesignPatters\SimpleFactory\Concretes;


class SimpleFactory
{
    public function createBicycle() : Bicycle
    {
        return new Bicycle();
    }
}


$factory = new SimpleFactory();
$bicycle = $factory->createBicycle();
$bicycle->driveTo();