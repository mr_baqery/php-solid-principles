<?php


namespace DesignPatters\StaticFactory\Tests;

use DesignPatters\StaticFactory\Concretes\FormatNumber;
use DesignPatters\StaticFactory\Concretes\FormatString;
use DesignPatters\StaticFactory\Concretes\StaticFactory;
use PHPUnit\Framework\TestCase;

class StaticFactoryTests extends TestCase
{
    public function testCanCreateNumberFormatter()
    {
        $this->assertInstanceOf(FormatNumber::class, StaticFactory::factory("number"));
    }

    public function testCanCreateStringFormatter()
    {
        $this->assertInstanceOf(FormatString::class, StaticFactory::factory("string"));
    }

}