<?php


namespace DesignPatters\StaticFactory\Concretes;


use DesignPatters\StaticFactory\Contracts\Formatter;

final class StaticFactory
{
    private const NUMBER = "number";
    private const STRING = "string";


    public static function factory(string $type) : Formatter
    {
        if ($type === StaticFactory::NUMBER){
            return new FormatNumber();
        }
        if ($type === StaticFactory::STRING)
        {
            return new FormatString();
        }
    }
}