<?php


namespace DesignPatters\StaticFactory\Concretes;


use DesignPatters\StaticFactory\Contracts\Formatter;

class FormatNumber implements Formatter
{

    public function format(string $input): string
    {
        return number_format((int)$input);
    }
}