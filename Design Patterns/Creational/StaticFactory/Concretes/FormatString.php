<?php


namespace DesignPatters\StaticFactory\Concretes;


use DesignPatters\StaticFactory\Contracts\Formatter;

class FormatString implements Formatter
{

    public function format(string $input): string
    {
        return $input;
    }
}