<?php


namespace DesignPatters\StaticFactory\Contracts;


interface Formatter
{
    public function format(string $input): string;
}