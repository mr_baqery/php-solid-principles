<?php


namespace DesignPatters\Prototype\Concretes;


use DesignPatters\Prototype\Contratcs\BookPrototype;

class FooBookPrototype extends BookPrototype
{
    protected string $category = "Foo";

    public function __clone()
    {
        // TODO: Implement __clone() method.
    }
}