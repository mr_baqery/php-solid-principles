<?php


namespace DesignPatters\Prototype\Concretes;


use DesignPatters\Prototype\Contratcs\BookPrototype;

class BarBookPrototype extends BookPrototype
{
    protected string $category = "Bar";

    public function __clone()
    {
        // TODO: Implement __clone() method.
    }
}