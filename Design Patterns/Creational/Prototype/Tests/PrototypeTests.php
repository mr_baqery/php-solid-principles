<?php


namespace DesignPatters\Prototype\Tests;

use DesignPatters\Prototype\Concretes\BarBookPrototype;
use DesignPatters\Prototype\Concretes\FooBookPrototype;
use PHPUnit\Framework\TestCase;

class PrototypeTests extends TestCase
{
    public function testCanGetBook()
    {
        $fooPrototype = new FooBookPrototype();
        $barPrototype = new BarBookPrototype();

        for($i = 0; $i < 10; $i++){
            $book = clone $fooPrototype;
            $book->setTitle("Foo Book No " . $i);
            $this->assertInstanceOf(FooBookPrototype::class, $book);
        }

        for($i = 0; $i < 10; $i++){
            $book = clone $barPrototype;
            $book->setTitle("Bar Book No " . $i);
            $this->assertInstanceOf(BarBookPrototype::class, $book);
        }



    }
}