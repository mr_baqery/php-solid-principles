<?php


namespace DesignPatters\Prototype\Contratcs;


abstract class BookPrototype
{
    protected string $title;

    protected string $category;

    abstract public function __clone();

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): string
    {
        $this->title = $title;
    }
}