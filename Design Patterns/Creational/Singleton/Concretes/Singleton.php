<?php


namespace DesignPatters\Singleton\Concretes;


final class Singleton
{
    private static ?Singleton $instance = null;


    /**
     * @return Singleton
     * gets an instance of Singleton class.
     * it it is null create new otherwise return existing
     */
    public static function getInstance() : Singleton
    {
        if (Singleton::$instance === null){
            Singleton::$instance = new Singleton();
        }

        return Singleton::$instance;
    }

    /**
     * is not allowed to call from outside to prevent from creating multiple instances,
     * to use the singleton, you have to obtain the instance from Singleton::getInstance() instead
     */
    private function __construct()
    {
    }

    /**
     * prevent the instance from being cloned.
     */
    private function __clone(): void
    {
        // TODO: Implement __clone() method.
    }

    /**
     * prevent from being unserialized (which would create a second instance of it)
     */
    private function __wakeup(): void
    {
        throw new \Exception("Cannot unserialize singleton");
    }


}