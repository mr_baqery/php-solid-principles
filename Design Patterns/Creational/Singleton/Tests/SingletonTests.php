<?php


namespace DesignPatters\Singleton\Tests;

use DesignPatters\Singleton\Concretes\Singleton;
use PHPUnit\Framework\TestCase;


class SingletonTests extends TestCase
{
    public function testUniqueness()
    {
        $firstCall = Singleton::getInstance();
        $secondCall = Singleton::getInstance();

        $this->assertInstanceOf(Singleton::class, $firstCall);
        $this->assertInstanceOf(Singleton::class, $secondCall);

        $this->assertSame($firstCall, $secondCall);
    }
}