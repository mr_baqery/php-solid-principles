<?php
// Violation of Single Responsibility law
class User
{

    protected function formatResponse($data)
    {
        return ["name" => $data->name, "email" => $data->email, ];
    }

    public function validateUser($user)
    {
        if ($user)
        {
            return true;
        }
        else
        {
            throw new Exception("Unknown type of model" . $e->getMessage());
        }
    }

    protected function fetchUserFromDatabase($userID)
    {
        return DB::findOrFail($userID);
    }

}

// Correct way
// But it still is violating some other priciples !
class User
{
    protected function findById($userID)
    {
        return (new DB()) ::findOrFail($userID);
    }

    protected function getUser($userId)
    {
        $user = $this->findById($userId);
        return (new FormatResponse())->format($user);
    }

    protected function validation()
    {
        return (new Validation())->validation();
    }
}

interface IDB
{
    public static function findOrFail($tableName);
}

class DB implements IDB
{
    public static function findOrFail($tableName)
    {
        // Some query fetching
        
    }

}

interface IValidation
{
    public function validation();
}

class Validation implements IValidation
{
    public function validation()
    {

    }
}

interface IResponse
{
    public function format(array $data);
}

class FormatResponse implements IResponse
{
    public function format(array $data)
    {
        // json response
        
    }
}

// Correct way
// Fixing dependency inversion problem
class User
{
    protected $response;
    protected $validation;
    protected $db;
    public function __construct(IResponse $response, IDB $db, IValidation $validation)
    {
        $this->response = $response;
        $this->validation = $db;
        $this->db = $validation;
    }

    protected function findById($userID)
    {
        return $this->db::findOrFail($userID);
    }

    protected function getUser($userId)
    {
        $user = $this->findById($userId);
        return $this->response->format($user);
    }

    protected function validation()
    {
        return $this->validation->validation();
    }
}

