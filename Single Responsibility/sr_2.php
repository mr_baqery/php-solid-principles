<?php

/**
 * A component or a class should have only one reason to change.
 * Cohesion : how much are methods of a class related together ?
 * High cohesion : the concept of all methods in a class is same
 *
 * Coupling : how much do two classes or two methods depend on each other ?
 * The less dependency, the more clean is the code.
 * Best Practice = High cohesion, Less dependency
 */


// False
class Register
{
    public function save($user)
    {
        // Four task !

        // Validation logic code

        // Db connections logic code

        // Insert into db code

        // return response code

    }
}


// Refactoring
class Register
{
    protected $validator;
    protected $db;
    protected $response;

    // inject dependency
    public function __construct(IValidation $validator, IDB $db, IResponse $response)
    {
        $this->validator = $validator;
        $this->db = $db;
        $this->response = $response;
    }

    public function save()
    {
        // there is no logic here, only the dependency have been injected.
        // Validation codes
        $this->validator->validateParams();

        // Db connections
        $this->db->connectToDb();

        // Insert into db
        $this->db->storeInDb();

        // return response
        $this->response->response();
    }
}


/* does this class have dependency
 to other classes?
 dependency degree = 1
 because it is coupled with $_POST super global.
 consider that we want to send
 data with another super global, say $_GET
 our class does not work anymore !
*/
class Validator
{
    public function validateParams(array $params)
    {
        // By using params, we can decouple
        // the super global

        // False
        if(!isset($_POST["firstname"])){
            return true;
        }

        // True
        if(!isset($param["firstname"])){
            return true;
        }
    }
}

class User
{
    public function storeInDb()
    {

    }

    // No logic, only instantiation.
    public function connectToDB()
    {
        return (new DBConfig())->connect();
    }
}

class DBConfig
{

    public function connect()
    {

    }
}