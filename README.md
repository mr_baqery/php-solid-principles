## SOLID Principles Examples in PHP
This repository contains comprehensive examples of solid principle.


## Single Responsibility Principle
A class should have one, and only one, reason to change. [Example](./Single%20Responsibility).
Each component in our app should only have one reason to change and one functionality and one task.

## Open Closed Principle
A class should be open for extension, but closed for modification. [Example](./Open-Closed%20Principles).
Our service should be extendable without changing previous codes. For example, if we have Notification service that sends SMS and Email, If we want to add TelegramSendMessage We should not change the previous codes. Our code should be easily plugin added.

## Liskov Substitution Principle
Derived classes must be substitutable for their base classes. [Example](./Liskov%20Substitution).
If we have an interface class, and there are some classes implement the interface, should be easily substitutable for their base class.

## Interface Segregation Principle
Many client-specific interfaces are better than one general-purpose interface. [Example](./Interface-Segregation-Principle).
If we have an interface class, all classes that implements the interface, should easily implement all methods stubs.

## Dependency Inversion Principle
Depend upon abstractions. Do not depend upon concretions. [Example](./Dependency-Inversion-Principle).
Instead of depending directly on a class, we should depend on interface class or abstract class. We should not use `new User()` expression, we should instead use "new HumanInterface"



#### For more information about *Object Oriented Design Principles*, you can refer [this slide](https://viblo.asia/thangtd90/posts/pVYRPJPmG4ng)
