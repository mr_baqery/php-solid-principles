<?php

// public
// protected
// private
// The state of our class should be changed outside of the class

class Wallet
{
    // Public ! False !
    public $balance;

    // True. write properties in private, protected
    // mode and write getter and setter for them.
    private $balance;

    // True
    public function getBalance()
    {
        return $this->balance;
    }

    // True
    public function chargeBalance(int $amount)
    {
        if($amount > 100){
            throw new Exception("amount is not valid");
        }
        $this->balance = $amount;
    }

    // True
    public function withdraw(int $amount)
    {
        if($amount > $this->balance){
            throw new Exception("amount not valid.");
        }
    }

    // The state of our class should be changed
    // with getter setter
}



// False
$wallet = new Wallet();
$wallet->balance = 25000;
