<?php


// Before
class Questionnaire
{

    // To many arguments !
    // The method is doing more than a job.
    public function __construct($firstName, $lastName, $city, $region, $phone, $email)
    {

    }
}



// After
class Questionnaire
{

    // To many arguments !
    // The method is doing more than a job.
    public function __construct(User $user, Address $address, Contact $contact)
    {

    }
}


class User
{
    public $firstName;
    public $lastName;
}

class Address
{
    public $city;
    public $region;
}

class Contact
{
    public $phone;
    public $email;
}

// What if we could not reduce the number of args ?
// use array instead.
// information var contains lastname, fname, phone, city, email , etc.
function register(array $information){
}

// The method's name should clearly show what it does.
class Notification
{
    // False
    public function handle($message, $user, $type = 'email')
    {

    }

    // True. because the api if the class is more readable.
    // default value parameters are also known as flag.
    // Avoid using flags(following signature is wrong).
    public function send($message,User $user, $type = 'email')
    {
        // False usage.
        if($user->status == 'active')
        {
            // send mail
        }

        // True. it's more readable.
        // and the encapsulation is done.
        // user's  property is inside of Class User.
        if($user->isActive()){

        }
    }

    // True.it's more readable.
    public function sendMail($message, $user)
    {

    }
    // True.it's more readable.
    public function sendSMS($message, $user)
    {
        # code...
    }


}



class User
{
    // False
    public $status; // active, inactive

    // True
    private $status; // active, inactive


    public function isActive()
    {
        retrun $this->status == 'active';
    }
}