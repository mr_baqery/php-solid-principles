<?php

// If another developer came into the project
// is it easy for him to understand our variable without
// seeing the right hand side of expression ?

// False
$myDate = date('Y-m-d');

// True
$currentDate = date('Y-m-d');

// False
$allowed = ['jpg', 'pdf'];

// True
$allowedFileExtension = ['jpg', 'pdf'];

// What is the data ?
$data = Videos::latest(10);

// True
$recentVideos = Videos::latest(10);

// False
$data = get('/users');

// True
$userPayload = get('/users');

// How to indicate a var is boolean ?
$isActive = true / false;

class User
{
    // False
    public $userFirstName;

    // True
    public $firstName;


    // False
    public function getUserFullName(Type $var = null)
    {
        # code...
    }

    // True
    public function getFullName(Type $var = null)
    {
        # code...
    }


}


// Type-hinting
function FunctionName(int $var = null)
{
    # code...
}

// Null coalescing operator
// if $var is null, then return 'value'
// else return $var
$var ?? 'value';

// False
if(!$isset($var)){
    return 'value';
}

// True
$name = $_GET['name'] ?? $_POST['name'] ?? 'reza';


// Do not use switch , else statement
// False
// How shall a developer know which gateway is for which Bank ?
// remember that we are not allowed to use comment.
class OnlinePayment
{

    const MELLAT_GATEWAY = 1;
    const PASSARGARD_GATEWAY = 2;
    const TEJARAT_GATEWAY = 3;

    public function pay($gateway)
    {
        if($gateway == self::MELLAT_GATEWAY){

        }
        if($gateway == self::PASSARGARD_GATEWAY){

        }
        if($gateway == self::TEJARAT_GATEWAY){

        }

    }
}