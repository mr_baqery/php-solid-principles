<?php

// Violation of Open-Closed principle law
// What if we want to add Facebook authentication method ? 
// we would add a new if-else block. so we have to edit our code.
class Login
{
    private function login($user)
    {
        if($user instanceof NormalUser)
        {
            // Login user with traditional method ie, username and password
        }
        elseif($user instanceof ThirdPartyUser)
        {
            // Login user with Google third-party lib.
        }
    }
}

// Correct way 

interface ILogin
{
    public function authenticateUser($user);
}

class NormaLogin implements ILogin
{
    public function authenticateUser($user)
    {
        // Authenticate user with username and password
    }
}

class ThirdPartyLogin implements ILogin
{
    public function authenticateUser($user)
    {
        // Authenticate user with google
    }
}

// Whenever we want to add another third-party authentication method,
// we just define it's class without chaning LoginModule class.
class LoginModule
{
    protected $login;

    public function __constructor(ILogin $login)
    {
        $this->login = $login;
    }

    public function login($user)
    {
        $this->login->authenticateUser($user);
    }
}