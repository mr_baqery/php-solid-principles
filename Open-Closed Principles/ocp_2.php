<?php


/**
 * A component
 * should be open for extension,
 * but closed for modification !
 */

// False
class Notification
{

    // It does three task, violates SRP.
    // also violates OCP.
    public function send($type, $params)
    {
        // $type == sms => sendSms()

        // $type == email => email()

        // $type == telegram => telegram()
    }

}


// True
class Notifaction
{
    // It does three task, violates SRP. Fixed -> No logic. just instantiation and dependency injection.
    // but still violates OCP.
    public function send($type, $params)
    {
        if($type == 'sms'){
            (new SMS())->send();
        }

        if($type == 'email'){
            (new Email())->send();
        }

        if($type == 'telegram'){
            (new Telegram())->send();
        }

    }
}



// True
class Notifaction
{
    // It does three task, violates SRP. Fixed -> No logic. just instantiation.
    // OCP fixed.
    // if we want to add Whatsapp notification,
    // this class does not need to be changed.
    public function send($className, $params)
    {
        return (new $className)->send($params);
    }
}