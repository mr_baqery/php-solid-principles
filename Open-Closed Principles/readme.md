## Open-Close Principle
It ensures that the code is open for extension but closed for modification.