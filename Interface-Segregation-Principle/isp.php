<?php 

// An example violating IS Principle.
interface CarInterface
{
    public function getFuel();

    public function shiftGear();

    public function steer();
}

class Car implements CarInterface
{
    public function getFuel()
    {
        // login to get fuel
    }

    public function shiftGear()
    {
        // logic to shift gear
    }

    public function steer()
    {
        // logic to steer
    }
}

class ElectricCar implements CarInterface
{
    public function getFuel()
    {
        // Electric car does not need to get fuel
        return null;
    }
    
    public function shiftGear()
    {
        // logic to shift gear
    }

    public function steer()
    {
        // logic to steer
    }
}

// Correct way
interface CarInterface
{
    public function getFuel();

    public function shiftGear();

    public function steer();
}

interface ElectricCarInterface
{
    public function shiftGear();

    public function steer();
}

class Car implements CarInterface
{
    public function getFuel()
    {
        // login to get fuel
    }

    public function shiftGear()
    {
        // logic to shift gear
    }

    public function steer()
    {
        // logic to steer
    }
}

class ElectricCar implements ElectricCarInterface
{
    public function shiftGear()
    {
        // logic to shift gear
    }

    public function steer()
    {
        // logic to steer
    }
}