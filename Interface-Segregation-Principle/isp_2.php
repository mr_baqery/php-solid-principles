<?php

// Interface should be small
// in order to client should easily
// implement it all it's stubs.


interface OnlinePaymentInterface
{
    public function pay();

}

interface mustBeVerified
{
    public function verify();
}

interface OfflinePaymentInterface
{

    public function pay();
}



class OnlinePayment implements OnlinePaymentInterface, mustBeVerified
{
    public function pay()
    {

    }

    public function verify()
    {

    }
}

/* in `cartToCart class we dont need to
 implement verify method.
 not implementing verify method
 violates LS principle.
 if we have not to implement some method,
 but actually we have the method stubs,
 we have violated the lsp.
 we should separate the interface
*/
class CartToCart implements OfflinePaymentInterface
{
    public function pay()
    {
        // logic
    }
}
