<?php

// An example violating DI Principle
interface DB
{
    public function connect();
}

class MySQL implements DB
{
    public function connect()
    {
        // Logic to connect to database
    }
}

interface Format
{
    public function formatResponse();
}

class JsonResponse implements Format
{
    public function formatResponse()
    {
        // logic to respond with json
    }
}

class Controller 
{
    public function connectDB()
    {
        return (new MySQL())->connect();
    }

    public function response()
    {
        return (new JsonResponse())->formatResponse();
    }
}


// Correct way
class Controller 
{
    protected $db;
    protected $response;

    public function __construct(DB $db, Format $response)
    {
        $this->db = $db;
        $this->response = $response;
    }

    public function connectDB()
    {
        return $this->db->connect();
    }

    public function response()
    {
        return $this->response->formatResponse();
    }
}


$controller = new Controller(new MySQL(), new JsonResponse());