## Dependency Inversion Principle

It states that High level modules should never depend on Low level modules, instead the High level module can depend upon an abstraction and the Low level module depends on that same abstraction. It’s not the simplest statement that we have come across. In very simple words… nope, a statement this complex can’t be simplified.