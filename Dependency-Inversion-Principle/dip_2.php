<?php


/*
Classes should depend on Abstractions, Not on Concretions.
Whenever we use new keyword we violate dependency inversion.
We should not depend on concrete, but abstract or interface.
If we change concrete class ,clients using the concrete class should not get stuck.
*/


interface Basket
{

}

class DatabaseBasket implements Basket
{

}


class SessionBasket implements Basket
{

}

// we eliminated the need of concrete class
class Order
{
    // inject dependency instead of
    // make new object right in the class.
    public function order(Basket $basket)
    {

    }
}

/*
At some point in our app,
we should declare the type of
concrete class. where should we declare ?
in IOC container which inject the dependency.
*/
// وظیفه این کانتینر این هست که هرجا که ما خواستیم که
// ابجکت از کلاسی که می خواهیم به ما بدهد.
// فقط داخل کانتینر می تونیم new بزنیم.

(new Order())->order(new SessionBasket());